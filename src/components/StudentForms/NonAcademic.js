import React, { useState } from "react";
import "./StudentForms.css";
import { Form, Row, Col, Button } from "react-bootstrap";
import { useForm } from "react-hook-form";

const NonAcademic = ({ setKey }) => {
  const { register, handleSubmit } = useForm();

  return (
    <div>
      <form>
        <Row>
          <Col className="my-3" lg={6}>
            <input
              name="languageKnown"
              placeholder="Language Known"
              ref={register}
            />
          </Col>
          <Col className="my-3" lg={6}>
            <input name="softSkills" placeholder="Soft Skills" ref={register} />
          </Col>
        </Row>
        <Row>
          <Col className="my-3" lg={6}>
            <input
              name="anySkill"
              placeholder="Any other Skills"
              ref={register}
            />
          </Col>
          <Col className="my-3" lg={6}>
            <input name="hobbies" placeholder="Hobbies" ref={register} />
          </Col>
        </Row>
      </form>
      <Row>
        <Col>
          <Button onClick={() => setKey("academic")}>Back</Button>
        </Col>
        <Col>
          <Button onClick={() => setKey("achievements")}>Next</Button>
        </Col>
      </Row>
    </div>
  );
};

export default NonAcademic;
