import React from "react";
import "./StudentForms.css";
import { Form, Row, Col, Button } from "react-bootstrap";
import { useForm } from "react-hook-form";

const Achievements = ({ setKey }) => {
  const { register, handleSubmit } = useForm();

  return (
    <div>
      <form>
        <Row>
          <Col className="my-3" lg={6}>
            <input
              name="achievement1"
              placeholder="Achievement 1"
              ref={register}
            />
          </Col>
          <Col className="my-3" lg={6}>
            <textarea
              name="achievementText1"
              placeholder="Something about the achievement"
              ref={register}
            />
          </Col>
        </Row>
        <Row>
          <Col className="my-3" lg={6}>
            <input
              name="achievement2"
              placeholder="Achievement 1"
              ref={register}
            />
          </Col>
          <Col className="my-3" lg={6}>
            <textarea
              name="achievementText2"
              placeholder="Something about the achievement"
              ref={register}
            />
          </Col>
        </Row>
      </form>
      <Row>
        <Col>
          <Button onClick={() => setKey("nonAcademicInfo")}>Back</Button>
        </Col>
        <Col>
          <Button>Finish</Button>
        </Col>
      </Row>
    </div>
  );
};

export default Achievements;
