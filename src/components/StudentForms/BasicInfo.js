import React, { useState } from "react";
import "./StudentForms.css";
import { Form, Row, Col, Button, FormControl } from "react-bootstrap";
import { useForm } from "react-hook-form";

const BasicInfo = ({ setKey }) => {
  const { register, handleSubmit } = useForm();

  const handleNext = () => {
    setKey("academic");
  };

  return (
    <div>
      <form onSubmit={handleSubmit(handleNext)}>
        <Row>
          <Col className="my-3" lg={6}>
            <input
              name="firstName"
              placeholder="First Name"
              ref={register({ required: true })}
            />
          </Col>
          <Col className="my-3" lg={6}>
            <input name="lastName" placeholder="Last Name" ref={register} />
          </Col>
        </Row>
        <Row>
          <Col className="my-3" lg={6}>
            <select name="gender" ref={register({ required: true })}>
              <option>Gender...</option>
              <option>Male</option>
              <option>Female</option>
            </select>
          </Col>
          <Col className="my-3" lg={6}>
            <input
              name="email"
              placeholder="Email"
              ref={register({ required: true })}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <Button type="submit">Next</Button>
          </Col>
        </Row>
      </form>
    </div>
  );
};

export default BasicInfo;
