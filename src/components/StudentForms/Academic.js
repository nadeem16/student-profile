import React, { useState } from "react";
import "./StudentForms.css";
import { Form, Row, Col, Button } from "react-bootstrap";
import { useForm } from "react-hook-form";

const Academic = ({ setKey }) => {
  const { register, handleSubmit } = useForm();

  const handleNext = () => {
    setKey("nonAcademicInfo");
  };

  return (
    <div>
      <form onSubmit={handleSubmit(handleNext)}>
        <Row>
          <Col className="my-3" lg={6}>
            <input
              name="XschoolName"
              type="text"
              placeholder="Class X School Name"
              ref={register({ required: true, minLength: 5 })}
            />
          </Col>
          <Col className="my-3" lg={6}>
            <input
              name="XMarks"
              type="number"
              placeholder="Class X obtained marks"
              ref={register({ required: true })}
            />
          </Col>
        </Row>
        <Row>
          <Col className="my-3" lg={6}>
            <input
              name="XIIschoolName"
              placeholder="Class XII School Name"
              ref={register({ required: true, minLength: 5 })}
            />
          </Col>
          <Col className="my-3" lg={6}>
            <input
              name="XIImarks"
              placeholder="Class XII obtained marks"
              ref={register({ required: true })}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <Button onClick={() => setKey("basicInfo")}>Back</Button>
          </Col>
          <Col>
            <Button type="submit">Next</Button>
          </Col>
        </Row>
      </form>
    </div>
  );
};

export default Academic;
