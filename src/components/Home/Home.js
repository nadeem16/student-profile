import React, { useState } from "react";
import "./Home.css";
import { Tabs, Tab, Container } from "react-bootstrap";
import Academic from "../StudentForms/Academic";
import Achievements from "../StudentForms/Achievements";
import BasicInfo from "../StudentForms/BasicInfo";
import NonAcademic from "../StudentForms/NonAcademic";

const Step = (props) => (
  <div className="Stepper_step">
    <div className="Stepper_indicator">
      <span className="Stepper_info">{props.indicator}</span>
    </div>
    <div className="Stepper_label">{props.title}</div>
    <div className="Stepper_panel">{props.children}</div>
  </div>
);

const Stepper = (props) => <div className="Stepper">{props.children}</div>;

const Home = () => {
  const [key, setKey] = useState("basicInfo");
  const [currentTab, setCurrentTab] = useState(1);

  const showActiveStep = (tabIndex) => {};

  return (
    <div>
      <Container>
        <h1>Student's Informations</h1>
        <Stepper currentTab={showActiveStep}>
          <Step indicator="1" title="Basic Info">
            <div className="Content">
              <BasicInfo />
            </div>
          </Step>
          <Step indicator="2" title="Academic">
            <div className="Content">
              <Academic />
            </div>
          </Step>
          <Step indicator="3" title="Non Academic">
            <div className="Content">
              <NonAcademic />
            </div>
          </Step>
          <Step indicator="4" title="Achievements">
            <div className="Content">
              <Achievements />
            </div>
          </Step>
        </Stepper>

        <Tabs id="controlled-tab" activeKey={key} onSelect={(k) => setKey(k)}>
          <Tab eventKey="basicInfo" title="Basic Info">
            <h1>Basic Info Tab</h1>
            <BasicInfo setKey={setKey} />
          </Tab>
          <Tab eventKey="academic" title="Academic">
            <h1>Academic Info Tab</h1>
            <Academic setKey={setKey} />
          </Tab>
          <Tab eventKey="nonAcademicInfo" title="Non Academic">
            <h1>Non Academic Info Tab</h1>
            <NonAcademic setKey={setKey} />
          </Tab>
          <Tab eventKey="achievements" title="Achievements">
            <h1>Achievements</h1>
            <Achievements setKey={setKey} />
          </Tab>
        </Tabs>
      </Container>
    </div>
  );
};

export default Home;
